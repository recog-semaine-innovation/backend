import { Injectable } from "@nestjs/common"
import {
  createWriteStream,
  readdirSync,
  unlinkSync,
  existsSync,
  mkdirSync,
} from "fs"
import * as archiver from "archiver"
import { basename } from "path"
import { Cron } from "@nestjs/schedule"
import { RecordingsService } from "../recordings/recordings.service"
import { SearchQueryDto } from "../recordings/dto/search-query.dto"

@Injectable()
export class FilesService {
  constructor(private readonly recordingsService: RecordingsService) {}

  async createArchive(searchQueryDto: SearchQueryDto): Promise<string> {
    const queryResult = searchQueryDto.ids
      ? await this.recordingsService.findByIds(searchQueryDto.ids.split(","))
      : await this.recordingsService.findByQuery(searchQueryDto)
    const src = queryResult.reduce((accumulator, currValue) => {
      return [...accumulator, ...currValue.src]
    }, [])

    if (!existsSync("./archives")) {
      mkdirSync("./archives")
    }

    const zipPathPromise = new Promise<string>(res => {
      const zipPath = `./archives/${new Date().getTime()}.zip`
      const output = createWriteStream(zipPath)
      const archive = archiver("zip", {
        zlib: { level: 9 }, // Sets the compression level.
      })

      // listen for all archive data to be written
      // 'close' event is fired only when a file is ready
      output.on("close", () => {
        res(zipPath)
      })

      archive.on("warning", err => {
        if (err.code === "ENOENT") {
          // log warning
        } else {
          // throw error
          throw err
        }
      })

      archive.on("error", err => {
        throw err
      })

      archive.pipe(output)

      src.forEach(source => {
        archive.file(source, { name: basename(source) })
      })

      archive.finalize()
    })

    return zipPathPromise
  }

  @Cron("0 0 * * *")
  cleanArchives(): void {
    const dir = "./archives"
    if (!existsSync(dir)) {
      return
    }
    const files = readdirSync(dir)
    files.forEach(file => {
      unlinkSync(file)
    })
  }
}
