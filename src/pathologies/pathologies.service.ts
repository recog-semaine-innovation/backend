import { Injectable } from "@nestjs/common"
import { InjectModel } from "@nestjs/mongoose"
import { Model } from "mongoose"
import { PathologyQueryDto } from "./dto/pathology-query.dto"
import { CreatePathologyDto } from "./dto/create-pathology.dto"
import { Pathology } from "./schemas/pathology.schema"

@Injectable()
export class PathologiesService {
  constructor(
    @InjectModel(Pathology.name) private pathologyModel: Model<Pathology>,
  ) {}

  findByQuery(pathologyQueryDto: PathologyQueryDto): Promise<Pathology[]> {
    return this.pathologyModel
      .find({ name: { $regex: pathologyQueryDto.name || "", $options: "i" } })
      .exec()
  }

  create(createPathologyDto: CreatePathologyDto): Promise<Pathology> {
    const createdPathology = new this.pathologyModel(createPathologyDto)
    return createdPathology.save()
  }

  async deleteById(_id: string): Promise<{ status: string }> {
    try {
      await this.pathologyModel.deleteOne({ _id })
      return { status: "success" }
    } catch (error) {
      throw new Error("failed deleting document")
    }
  }
}
