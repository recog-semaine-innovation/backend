import { Test, TestingModule } from "@nestjs/testing"
import { PathologiesController } from "./pathologies.controller"

describe("Pathologies Controller", () => {
  let controller: PathologiesController

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [PathologiesController],
    }).compile()

    controller = module.get<PathologiesController>(PathologiesController)
  })

  it("should be defined", () => {
    expect(controller).toBeDefined()
  })
})
