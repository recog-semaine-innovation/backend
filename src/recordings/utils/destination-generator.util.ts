import { Request } from "express"
import { existsSync, mkdirSync } from "fs"
import { HttpException, HttpStatus } from "@nestjs/common"
import { extname } from "path"
import { File } from "../interfaces/file.interface"

export const destinationGenerator = (
  req: Request,
  file: File,
  callback: (arg, name) => void,
): void => {
  const { uid } = req.body
  const dir = `./uploads/${uid}/`

  const extension = extname(file.originalname)
  if (extension !== ".mp3") {
    callback(
      new HttpException("Only .mp3 files are accepted", HttpStatus.FORBIDDEN),
      false,
    )
  }

  if (!existsSync("./uploads")) {
    mkdirSync("./uploads")
  }

  if (!existsSync(dir)) {
    mkdirSync(dir)
  }

  callback(null, dir)
}
