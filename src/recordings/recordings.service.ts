import { Injectable, Logger } from "@nestjs/common"
import { InjectModel } from "@nestjs/mongoose"
import { Model } from "mongoose"
import * as ffmpeg from "ffmpeg"
import { unlinkSync, existsSync, rmdirSync, statSync } from "fs"
import { SearchQueryDto } from "./dto/search-query.dto"
import { File } from "./interfaces/file.interface"
import { CreateRecordingDto } from "./dto/create-recording.dto"
import { Recording } from "./schemas/recording.schema"

@Injectable()
export class RecordingsService {
  constructor(
    @InjectModel(Recording.name) private recordingModel: Model<Recording>,
  ) {}

  findByQuery(searchQueryDto: SearchQueryDto): Promise<Recording[]> {
    const query = this.recordingModel.find()
    if (searchQueryDto.pathology) {
      query.find({
        pathology: { $regex: searchQueryDto.pathology, $options: "i" },
      })
    }
    if (searchQueryDto.order) {
      query.sort({ date: searchQueryDto.order })
    }
    if (searchQueryDto.startDate) {
      query.where("date").gte(searchQueryDto.startDate)
    }
    if (searchQueryDto.endDate) {
      query.where("date").lte(searchQueryDto.endDate)
    }
    return query.exec()
  }

  findByUid(uid: string): Promise<Recording[]> {
    return this.recordingModel.find({ uid }).exec()
  }

  findById(_id: string): Promise<Recording[]> {
    return this.recordingModel.find({ _id }).exec()
  }

  findByIds(ids: string[]): Promise<Recording[]> {
    Logger.warn(ids)
    return this.recordingModel.find({ _id: { $in: ids } }).exec()
  }

  async create(
    createRecordingDto: CreateRecordingDto,
    files: File[],
  ): Promise<Recording> {
    try {
      const src = await this.convertToMp3(files.map(file => file.path))
      const newRecording = {
        ...createRecordingDto,
        src,
      }
      const createdRecording = new this.recordingModel(newRecording)
      return await createdRecording.save()
    } catch (error) {
      Logger.error(error)
      throw new Error(error)
    }
  }

  async deleteById(_id: string): Promise<{ status: string }> {
    try {
      const { src } = await this.recordingModel.findById(_id)
      const dir = "./uploads"
      src.forEach(file => {
        if (existsSync(file)) {
          unlinkSync(file)
        }
      })
      rmdirSync(`${dir}/${src[0].split("/")[1]}`)
    } catch (error) {
      Logger.error(error)
    }

    try {
      await this.recordingModel.deleteOne({ _id })
      return { status: "success" }
    } catch (error) {
      Logger.error(error)
      return { status: "failed" }
    }
  }

  async convertToMp3(files: string[]): Promise<string[]> {
    const audioPromises = []
    for (let index = 0; index < files.length; index++) {
      const file = files[index]
      const process = new ffmpeg(file)
      audioPromises.push(process)
    }
    const audioObjects = await Promise.all(audioPromises)
    const src = audioObjects.map(
      (audio, index) =>
        new Promise<string>(res => {
          audio.fnExtractSoundToMP3(`${files[index]}.mp3`, (err, fileSrc) => {
            if (!err) {
              try {
                unlinkSync(files[index])
              } catch (e) {
                Logger.error(e)
              }
              res(fileSrc)
            }
          })
        }),
    )

    return Promise.all(src)
  }
}
