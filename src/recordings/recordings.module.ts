import { MongooseModule } from "@nestjs/mongoose"
import { Module } from "@nestjs/common"
import { Recording, RecordingSchema } from "./schemas/recording.schema"
import { RecordingsController } from "./recordings.controller"
import { RecordingsService } from "./recordings.service"

@Module({
  imports: [
    MongooseModule.forFeature([
      { name: Recording.name, schema: RecordingSchema },
    ]),
  ],
  providers: [RecordingsService],
  controllers: [RecordingsController],
  exports: [RecordingsService],
})
export class RecordingsModule {}
