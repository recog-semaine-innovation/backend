import { Prop, Schema, SchemaFactory } from "@nestjs/mongoose"
import { Document } from "mongoose"

@Schema()
export class Recording extends Document {
  @Prop({ required: true })
  uid: string

  @Prop({ required: true })
  src: string[]

  @Prop({ required: true })
  pathology: string

  @Prop({ default: new Date().getTime() })
  date: number
}

export const RecordingSchema = SchemaFactory.createForClass(Recording)
