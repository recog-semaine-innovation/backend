import { Module } from "@nestjs/common"
import { MongooseModule } from "@nestjs/mongoose"
import { ConfigModule } from "@nestjs/config"
import { AppController } from "./app.controller"
import { AppService } from "./app.service"
import { RecordingsModule } from "./recordings/recordings.module"
import { FilesModule } from "./files/files.module"
import { PathologiesModule } from "./pathologies/pathologies.module"

@Module({
  imports: [
    RecordingsModule,
    ConfigModule.forRoot(),
    FilesModule,
    MongooseModule.forRoot(process.env.MONGO_URL || "mongodb://localhost/nest"),
    PathologiesModule,
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
